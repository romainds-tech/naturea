<?php

use App\Http\Controllers\Dataplants;
use App\Http\Controllers\DataplantsController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth/login');
});

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', [DataplantsController::class, 'index'])->name('dashboard');

Route::middleware(['auth:sanctum', 'verified'])->get('/datacloud/{id}', [DataplantsController::class, 'getPlantData'])->name('datacloud');
Route::middleware(['auth:sanctum', 'verified'])->post('/updatedata/{id}', [DataplantsController::class, 'storeData'])->name('updatedata');


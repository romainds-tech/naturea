<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Dataplants;
use App\Models\Plants;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class DataplantsController extends Controller
{
    public function index(){
        $plants = Dataplants::all();

        return view('dashboard', ['plants' => $plants]);
    }

    public function getPlantData($id){
        $plants = Dataplants::all();
        $plant = $plants->find($id);
        $plantsentries = Plants::all();
        
        return view('plant', ['plant' => $plant, 'plantsentries' => $plantsentries]);
    }

    public function storeData($id){

        $plants = Dataplants::all();
        $plant = $plants->find($id);
        
        
        $data = Plants::all();
        $datas = $data->find($id);

        $plantsentries = Plants::all();

        date_default_timezone_set('UTC');

        // Date de départ des bdd
        $date = "2021-09-25";
        $i=0;
        $pass = True;
        // boucle pour récupérer les table
        while (date("Y-m-d") >= $date) {
    
            if($i>=5){
                sleep(1);
                $i=0;
            }
    
            $lienbdd = "iotp_qq7oqf_all-data_" . $date;
            $url = "https://apikey-v2-2e1rk04u4k6jq5gsu0k5g01rh2f801bikyx5zdye9f71:164cfb25d1b199b4e18e240a5e53e8d6@342dcd3b-55a7-43be-96fd-769fc47e3e41-bluemix.cloudantnosqldb.appdomain.cloud/" . $lienbdd . "/_all_docs?include_docs=true";
    
            $ch = curl_init();
    
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    
            //Saisir l'URL et la transmettre à la variable.
            curl_setopt($ch, CURLOPT_URL, $url);
            //Désactiver la vérification du certificat puisque waytolearnx utilise HTTPS
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            //Exécutez la requête 
            $result = curl_exec($ch);
            //Afficher le résultat
            $tableau = json_decode($result, true);
    
    
            // test si bdd éxiste
            if (empty($tableau["error"])) {
                $bdd = array();
                // Si bdd est remplis = découpage
                if ($tableau["rows"][0]["id"] != "_design/iotp") {
                    foreach (range(0, count($tableau["rows"]) - 1) as $number) {
                        if (isset($tableau["rows"][$number]['doc']['timestamp']) && $tableau["rows"][$number]['doc']['data']) {
                            $bdd[] = array(
                                "id" => $number,
                                "timestamp" => $tableau["rows"][$number]['doc']['timestamp'],
                                'data' => $tableau["rows"][$number]['doc']['data'],
                            );
                            foreach ($plantsentries as $plantentrie) {
                                if ($plantentrie->date == $bdd[0]['timestamp']){
                                    $pass = False;
                                    break;
                                }
                                else{
                                    $pass = True;
                                }
                                    
                            }
                            
                            if($pass == True){
                                DB::table('plants')->insert([
                                    'temperature' => $bdd[0]['data']['temperature'],
                                    'humidity' => $bdd[0]['data']['humidite'],
                                    'brightness' => $bdd[0]['data']['luminosite'],
                                    'date' => $bdd[0]['timestamp'],
                                    'dataplants_id' => $id
                                ]);
                            }
                            
                        }
                    }
                }else{
                    $bdd = array();
                }
            }
            else {
                $bdd = array();
            }
            // var_dump($bdd);
            $tableauAllTable[] = array($date => $bdd);
    
            // modification date pour récuperer la bdd
            $date = date('Y-m-d', strtotime($date . ' + 1 days'));
            $i++;
        }

        

        return redirect()->route('datacloud', ['id' => $id]);
    }
}


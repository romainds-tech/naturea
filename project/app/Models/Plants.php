<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Plants extends Model
{
    use HasFactory;
    protected $table = 'plants';


    /**
     * @var string
     */
    protected $fillable = [
        'dataplant_id',
        'temperature',
        'humidity',
        'brightness',
        'date',
    ];



    public function plant()
    {
        return $this->belongsTo(Dataplants::class);
    }




    public $timestamps = false;
}

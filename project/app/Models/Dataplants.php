<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Dataplants extends Model
{
    use HasFactory;
    protected $table = 'dataplants';


    /**
     * @var string
     */
    protected $fillable = [
        'name',
        'temperature',
        'humidity',
        'brightness',
        'url'
    ];



    public function user()
    {
        return $this->belongsTo('App\Models\User','id');
    }




    public $timestamps = false;
}

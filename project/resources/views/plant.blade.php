<x-app-layout>
    <div>
        <div style="padding: 50px;">
            <h1 style="text-align: center; padding:10px; font-size: 30px;">{{ $plant->name }}</h1>
            <img src="../../images/{{ $plant->url }}" style="width:300px; height:auto; margin: 0 auto; padding:10px;">

            <div>
                <h3 style="text-align: center; padding:10px;" >Conditions idéales</h3>
                <div style="width:350px; margin: 0 auto; padding:10px;">
                    <div style="width: 100%;"> Températures : {{ $plant->temperature }} °C</div>
                    <div style="width: 100%;"> Humidité : {{ $plant->humidity }} %</div>
                    <div style="width: 100%;"> Luminosité : {{ $plant->brightness }}  Lux</div>
                </div>
            </div>

            <div>
                <h3 style="text-align: center; padding:10px;" >Données en temps réels du capteur</h3>
                    <div style="width:350px; margin: 0 auto; padding:10px;">
                        <div style="width: 100%;"> Températures : <div id="tempcapt" style="display: inline-block;"></div> °C</div>
                        <div style="width: 100%;"> Humidité : <div id="humiditycapt" style="display: inline-block;"></div> %</div>
                        <div style="width: 100%;"> Luminosité : <div id="luxcapt" style="display: inline-block;"></div>  Lux</div>
                    </div>
            </div>

            <div>
                

                    <form method="POST" action="{{ route('updatedata', ['id' => $plant->id]) }}">
                    @csrf

                    <h3 style="text-align: center; padding:10px;" >Historique des données</h3>

                    <div class="flex items-center justify-center mt-4">
                        
                        <x-jet-button class="ml-4">
                        {{ __('Mettre à jour les données') }}   
                        </x-jet-button>
                    </div>
                    </form>

                    <div style="width: 600px; margin: 0 auto; padding-top:20px;">
                        @if ($plantsentries != null)
                        <table>
                                <tr>
                                    <th>Temperature</th>
                                    <th>Humidité</th>
                                    <th>Luminosité</th>
                                    <th>Date</th>
                                </tr>
                            @foreach ($plantsentries as $plantsentrie)
                
                                <tr>
                                    <td>{{ $plantsentrie->temperature }}</td>
                                    <td>{{ $plantsentrie->humidity }}</td>
                                    <td>{{ $plantsentrie->brightness }}</td>
                                    <td>{{ substr($plantsentrie->date, 0, -14) }}</td>
                                </tr>
                                
                            @endforeach
                        </table>
                        @endif

                    </div>

                   


            </div>
        </div>


    </div>
    <style>
table {
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

td, th {
  border: 1px solid #dddddd;
  text-align: left;
  padding: 8px;
}

tr:nth-child(even) {
  background-color: #dddddd;
}
</style>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/paho-mqtt/1.0.1/mqttws31.js" type="text/javascript"></script>

<script>
    // Changer vos paramètres
    const myOrg = "qq7oqf"
    const typeId = "wipy";
    const deviceId = "b0xaexa4Oxf8xac";

    var tempcapt = document.getElementById("tempcapt");
    var humiditycapt = document.getElementById("humiditycapt");
    var luxcapt = document.getElementById("luxcapt");



    function create_UUID() {
        var dt = new Date().getTime();
        var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
            var r = (dt + Math.random() * 16) % 16 | 0;
            dt = Math.floor(dt / 16);
            return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16);
        });
        return uuid;
    }

    const clientId = 'a:' + myOrg + ':' + create_UUID();

    client = new Paho.MQTT.Client(myOrg + ".messaging.internetofthings.ibmcloud.com", 443, "", clientId);

    // set callback handlers
    client.onConnectionLost = function (responseObject) {
        console.log("Connection Lost: " + responseObject.errorMessage);
    }

    client.onMessageArrived = function (message) {
        // console.log("Message Arrived: " + message.payloadString)
        const tableau = JSON.parse(message.payloadString)
        for(let key in tableau) {
            console.log(key + ":" + tableau[key]);
        }
        tempcapt.innerHTML = tableau['temperature'];
        humiditycapt.innerHTML = tableau['humidite'];
        luxcapt.innerHTML = tableau['luminosite'];
        // out_msg="Message"+message.payloadString+"<br>";
        // out_msg=out_msg+"msg topic"+message.topic;
        // console.log(out_msg);
    }

    
    



    // Topic pour accéder aux données
    const topic = 'iot-2/type/' + typeId + '/id/' + deviceId + '/evt/status/fmt/json';
    console.log(topic);

    // Called when the connection is made
    function onConnect() {
        console.log("Connected!");
        client.subscribe(topic);
    }

    function onConnectF() {
        console.log(" not Connected!");
    }


    // Changer vos paramètres de connexion
    client.connect({
        onSuccess: onConnect,
        onFailure: onConnectF,
        userName: "a-qq7oqf-seuesleuwe",
        password: "lFam-flgc3&o&(SoCM",
        useSSL: true
    });
</script>

</x-app-layout>

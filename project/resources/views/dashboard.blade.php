<x-app-layout>
<!DOCTYPE html>
    <html>
       

        <body>
            @section('content')
            {{-- <div class="banner">
                
                <div class="banner-text">
                    <h1>
                        <strong>NATUREA</strong>
                    </h1>
                    <h2>L'iot au service de la nature<h4>
                </div>
            </div> --}}
            <div class="pres">
                <img class="rose" src="/images/logo_naturea.svg" alt="">
                <h1 class="title">NATUREA</h1>
                <h2 class="baseline">L'iot au service de la nature</h2>
                <div class="gsquare"></div>
                <img class="plantpng" src="/images/image_2.png" alt="">
                <button class="start">Débuter</button>
                <img class="arrow" src="/images/arrow.png" alt="">
            </div>

            <!-- <div id="jpj">
                <div class="container">
                    <div class="jour">
                        <div class="jour-titre">
                            <h3 class="day-number">JOUR N°XXX</h3>
                            <h4 class="date">02/10/2021</h4>
                        </div>
                        <img class="jour-img" src="images/photo1.svg">
                        <div class="jour-info">
                            <p>Température : Entre XX° et XX°</p>
                            
                            <p>Lumière : Active de XX heure à XX heure</p>
     
                            <p>Humidité : Blabla</p>
                        </div>
                    </div>
                </div>
            </div> -->

            <div id="Mes plantes">
                <div class="container2">
                    <h3 class="pictures-title">Mes plantes</h3>
                    <div class="imgs">
                        <div class = "ligne1">
                            @foreach ($plants as $plant)
                                <a href="{{route('datacloud', ['id' => $plant->id])}}"><img src='images/{{ $plant->url }}' style="width:250px; height:250px;"></img></a>
                            @endforeach
                            <img src="images/photo2.svg" width = 250 height = 250></img>
                            <img src="images/photo3.svg" width = 250 height = 250></img>
                        </div>
                        <div class ="ligne2">
                            <img src="images/photo4.svg" width = 250 height = 250></img>
                            <img src="images/photo5.svg" width = 250 height = 250></img>
                            <img src="images/photo6.svg" width = 250 height = 250></img>
                        </div>
                        <div class ="ligne3">
                            <img src="images/photo7.svg" width = 250 height = 250></img>
                            <img src="images/photo8.svg" width = 250 height = 250></img>
                            <img src="images/photo9.svg" width = 250 height = 250></img>
                        </div>
                </div>
            </div>

            <footer>
                ©NATUREA
            </footer>

<style type="text/css">
.pres{
    position: relative;
    display: flex;
    width: 100%;
}
.gsquare{
    width: 50%;
    background-color: #6CBF8E;
}
.plantpng{
    width: 50%;
}
.baseline{
    position: absolute;
    font-family: Rubik Mono One;
    color: #DCF9E8;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    font-size: 24px;
    width: 500px;
    text-align: center;
    background-color: #6CBF8E;
}
.title{
    position: absolute;
    font-family: Rubik Mono One;
    color: #DCF9E8;
    text-shadow: 0px 12px 0px #616B65;
    top: 40%;
    left: 50%;
    transform: translate(-50%, -50%);
    font-size: 64px;
}
.rose{
    position: absolute;
    top: 38%;
    left: 32%;
    transform: translate(-50%, -50%);
}
.start{
    position: absolute;
    width: 200px;
    height: 70px;
    background: #DCF9E8;
    border-radius: 24px;
    top: 70%;
    left: 50%;
    transform: translate(-50%, -50%);
    font-family: Luckiest Guy;
    color: #404040;
    font-size: 24px;
}
.arrow{
    position: absolute;
    top: 88%;
    left: 50%;
    transform: translate(-50%, -50%);
}
html::-webkit-scrollbar {
  width: 15px;
}
html::-webkit-scrollbar-track {
  background: #404040;
}
::-webkit-scrollbar-thumb {
  background: #DCF9E8;
}
html::-webkit-scrollbar-thumb:hover {
  background: #6CBF8E;
}
p{
    font-family: Rubik Mono One;
}
h3, h4{
    font-family: Luckiest Guy;
}
/* .banner{
    position:relative;
    display:flex;
    width:100%;
    height: 88.6vh;
    background: linear-gradient(rgba(0, 0, 0, 0.45),
        rgba(0, 0, 0, 0.45)), url(/images/bg1.png);
    background-attachment: fixed;
    background-size: cover;
}

.banner-text{
    display: block;
    text-align: center;
    margin: auto;
    color: white
}

.banner-text>h1{
    font-size : 5vw;
}

.banner-text>h2{
    font-size: 1.5vw;
} */

.container{
    min-height: 0vh;
    height : 100vh;
    width:100%;
    display:flex;
    align-items:center;
    justify-content: center;
    background-color : #EDFFF4;
}
.jour{
    background-color : #DCF9E8;
    width : 80%;
    height : 60vh;
    border-radius: 30px;
    box-shadow: -20px 20px #95CAAB;
    padding : 20px;
    display:flex;
    flex-direction: row;
}
.jour-img{
    width: 300px;
    margin-left: 60px;
}
.date{
    color: #6CBF8E;
    font-size: 24px;
    text-align: right
}
.day-number{
    font-size: 30px;
    color: #404040;
}
.jour-info{
    margin-top : 70px;
    margin-left: 25px;
    color: #404040;
}
.jour-info p{
    margin-top: 50px;
}
.pictures-title{
    font-family: Rubik Mono One;
    color: #DCF9E8;
    text-align: left !important;
    font-size: 24px;
}
.container2{
    min-height: 0vh;
    width:100%;
    display:flex;
    flex-direction: column;
    align-items:center;
    justify-content: center;
    background-color : #6CBF8E;
}

.container2 h3{
    margin-top : 40px;
}

.imgs{
    display:flex;
    flex-direction: column;
}

.ligne1{
    flex: auto;
    display:flex;
    flex-direction: row;
}

.ligne2{
    flex: auto;
    display:flex;
    flex-direction: row;
}

.ligne3{
    flex: auto;
    display:flex;
    flex-direction: row;
}

.imgs img{
    margin : 20px;
    border-radius : 40px;
    box-shadow: -10px 10px #a9f6c8;
    transition: 0.3s;
}

.imgs img:hover {
    box-shadow: -20px 20px #a9f6c8;
}


footer{
    height : 15vh;
    width:100%;
    margin-top : auto;
    padding : 30px;
    letter-spacing : 2px;
    transition : 0.6s;
    text-align: center;
    box-shadow : 4px 1px 4px 4px lightgrey;
    background : white;
    margin-top : 40px;
}

</style>



        </body>
    </html>
</x-app-layout>



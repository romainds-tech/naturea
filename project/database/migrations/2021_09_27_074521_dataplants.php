<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Dataplants extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('dataplants', function (Blueprint $table) {
          $table->id();
          $table->string('name');
          $table->string('temperature');
          $table->string('humidity');
          $table->string('brightness');
          $table->string('url');
          $table->foreignId('user_id')->constrained('users');
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        schema::dropIfExists('dataplants');
    }
}

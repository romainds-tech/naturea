<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Dataplants;

class Dataplantsseeders extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('dataplants')->insert([
            'name' => "Chénopode bon-Henri",
            'temperature' => 25.5,
            'humidity' => 60,
            'brightness' => 2000,
            'url' => "henri.jpg",
            'user_id' => (1)
        ]);


    }
}
